/* 
 seit visi mani skripti
 */

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function gen_pieprasijumus(){
    var max_robeza = document.getElementById("max_robeza").value;
    var cnt = document.getElementById("gen_cnt").value;
    
    var rez = [];
    
    if(cnt==""){
        //netika ievadits skaits
        document.getElementById("err_cnt").innerHTML=
                '<div class="alert alert-danger alert-dismissable"> <button type="button" \n\
                class="close" data-dismiss="alert">&times;</button><p>Netika ievadīts skaits, cik ģenerēt!</p> </div>';
        return;
    }
    
    for(i=0; i<cnt; ++i){
        rez.push(getRandomInt(0,max_robeza).toString());
    }
    document.getElementById("starta_poz").value = getRandomInt(0,max_robeza).toString();
    document.getElementById("pieprasijumi").value=rez;
    
    
}//end of gen_pieprasijumus


function start_vis(){
    
    
    var start_poz, pieprasijumi=[];
    
    var starta_poz = parseInt(document.getElementById("starta_poz").value);
    var pieprasijumi_str_val = document.getElementById("pieprasijumi").value.split(",");
    var cnt = pieprasijumi_str_val.length;
    var max_robeza = document.getElementById("max_robeza").value;
    
    if(isNaN(starta_poz)|| starta_poz!=document.getElementById("starta_poz").value || starta_poz<0 || starta_poz>max_robeza){
        document.getElementById("err_dati").innerHTML=
                '<div class="alert alert-danger alert-dismissable"> <button type="button" \n\
                    class="close" data-dismiss="alert">&times;</button><p>Netika ievadīta korekta starta poz!'+document.getElementById("starta_poz").value+'</p> </div>';
        return;
    }
    
    for(i=0; i<cnt; ++i){
        var s=pieprasijumi_str_val[i];
        var x = parseInt(s);
        if(isNaN(x) || x!=s || x>max_robeza || x<0){
            document.getElementById("err_dati").innerHTML=
                '<div class="alert alert-danger alert-dismissable"> <button type="button" \n\
                    class="close" data-dismiss="alert">&times;</button><p>Nekorekti pieprasījumi! '+s+' </p> </div>';
        return;
        }
        pieprasijumi.push(x);
    }
    //viss ir OK - zime grafiku
    myChartDraw(starta_poz, pieprasijumi);
    
    
}//end of start


function find_fcfs_data(starta_poz, pieprasijumi){
    var rez = [starta_poz].concat(pieprasijumi);
    return rez;
}//end of find_fcfs_data

function find_sstf_data(starta_poz, pieprasijumi){
    var rez=[starta_poz];
    var len=pieprasijumi.length;
    
    var taken=[];
    var last=starta_poz;
    var min_dist, min_dist_ind;
    
    for(i=0;i<len;++i)taken[i]=false;
    
    for(k=0; k<len; ++k){
        min_dist=-1;
        min_dist_ind=-1;
        for(i=0; i<len; ++i){
            if(!taken[i] &&(min_dist===-1 || Math.abs(pieprasijumi[i]-last)<min_dist)){
                min_dist = Math.abs(pieprasijumi[i]-last);
                min_dist_ind = i;
            }
        }
        
        last = pieprasijumi[min_dist_ind];
        rez.push(last);
        taken[min_dist_ind] = true;
    }
    return rez;
}//end of find_sstf_data



function find_scan_data(starta_poz, pieprasijumi){
    var rez=[starta_poz];
    var len=pieprasijumi.length;
    var max_robeza = document.getElementById("max_robeza").value;
    
    var temp = [].concat(pieprasijumi);
    temp = temp.sort(function(l,r){return l>r;});
    
    ind=0;
    while(ind<len && starta_poz>temp[ind]){
        ind++;
    }
    
    if(max_robeza-starta_poz<=starta_poz){
        // no sākuma iet pa labi
        for(i=ind; i<len; ++i)rez.push(temp[i]);
        if(temp[len-1]<max_robeza)rez.push(max_robeza);
        
        //tad iet pa kreisi
        for(i=ind-1; i>=0; --i)rez.push(temp[i]);
    }
    else{
        //no sākuma iet pa kreisi
        for(i=ind-1; i>=0; --i)rez.push(temp[i]);
        if(temp[0]>0)rez.push(0);
        //tad iet pa labi
        for(i=ind; i<len; ++i)rez.push(temp[i]);
    }
    
    
    return rez;
}//end of find_scan_data


function find_cscan_data(starta_poz, pieprasijumi){
    var rez=[starta_poz];
    var len=pieprasijumi.length;
    var max_robeza = parseInt(document.getElementById("max_robeza").value);
    
    var temp = [].concat(pieprasijumi);
    temp = temp.sort(function(l,r){return l>r});
    
    ind=0;
    while(ind<len && starta_poz>temp[ind]){
        ind++;
    }
    
    if(max_robeza-starta_poz<=starta_poz){
        // iet pa labi
        for(i=ind; i<len; ++i)rez.push(temp[i]);
        if(temp[len-1]<max_robeza)rez.push(max_robeza);
        
        if(ind>0 && ind<max_robeza)rez.push(null);
        
        if(temp[0]>0)rez.push(0);
        //tad aizlec un atkal iet pa labi
        for(i=0; i<ind; ++i)rez.push(temp[i]);
    }
    else{
        //iet pa kreisi
        for(i=ind-1; i>=0; --i)rez.push(temp[i]);
        if(temp[0]>0)rez.push(0);
        
        if(ind>0 && ind<max_robeza)rez.push(null);
        
        if(temp[len-1]<max_robeza)rez.push(max_robeza);
        //tad iet pa labi
        for(i=len-1; i>=ind; --i)rez.push(temp[i]);
    }
    return rez;
}//end of find_cscan_data


function find_clook_data(starta_poz, pieprasijumi){
    var rez=[starta_poz];
    var len=pieprasijumi.length;
    var max_robeza = parseInt(document.getElementById("max_robeza").value);
    
    var temp = [].concat(pieprasijumi);
    temp = temp.sort(function(l,r){return l>r});
    
    ind=0;
    while(ind<len && starta_poz>temp[ind]){
        ind++;
    }
    
    if(temp[len-1]-starta_poz<=starta_poz){
        // iet pa labi
        for(i=ind; i<len; ++i)rez.push(temp[i]);
        
        if(ind>0 && ind<max_robeza)rez.push(null);
        
        //tad aizlec un atkal iet pa labi
        for(i=0; i<ind; ++i)rez.push(temp[i]);
    }
    else{
        //iet pa kreisi
        for(i=ind-1; i>=0; --i)rez.push(temp[i]);
        
        if(ind>0 && ind<max_robeza)rez.push(null);
        
        //tad iet pa labi
        for(i=len-1; i>=ind; --i)rez.push(temp[i]);
    }
    return rez;
}//end of find_clook_data









function find_total_dist(arr){
    var dist=0;
    var len=arr.length;
    
    for(i=1;i<len;++i){
        if(arr[i]!==null){
            dist+=Math.abs(arr[i]-arr[i-1]);
        }
        else{
            i++;//lai izlaistu punktu
        }
    }
    return dist;
}


function find_total_dist_arr(arr){
    var dist_arr=[0];
    var dist=0;
    var len=arr.length;
    
    for(i=1;i<len;++i){
        if(arr[i]!==null){
            dist+=Math.abs(arr[i]-arr[i-1]);
            dist_arr.push(dist);
        }
        else{
            dist_arr.push(dist);
            i++;
        }
        
    }
    return dist_arr;
}


function add_x_koord(data){
    var len = data.length;
    var rez = [];
    var ind=0;
    for(i=0; i<len; ++i){
        if(data[i]!==null){
            rez.push({x:ind,y:data[i]});
            ind++;
        }
        else{
            ind--;
            rez.push({x:ind,y:null});
        }
    }
    
    return rez;
}



function myChartDraw(starta_poz, pieprasijumi){
    
    
    //ar šo izmet iepriekšējās chart
    var my_chart_container = document.getElementById("my_chart_container");
    my_chart_container.className="";
    my_chart_container.innerHTML = '&nbsp;';
    my_chart_container.innerHTML = '<canvas id="my_scatter_chart_canvas"></canvas><br><br>\n\
                                                                   <canvas id="my_bar_chart_canvas"></canvas><br><br>\n\
                                                                   <canvas id="my_scatter_chart2_canvas"></canvas>';
    
    
    
    var ctx_scatter_chart = $("#my_scatter_chart_canvas"); //tas pats kas document.getElementById
    var ctx_bar_chart = $("#my_bar_chart_canvas"); //tas pats kas document.getElementById
    var ctx_scatter_chart2 = $("#my_scatter_chart2_canvas");
    
    var N = pieprasijumi.length;
    
    var my_scatter_chart_data_sets=[];
    var my_scatter_chart2_data_sets=[];
    
    
    
    var my_bar_chart_data_sets=[];
    
    
        
    
    
    if($("#FCFS_alg_chk_box").prop("checked")){
        //ja ir atzimets FCFS, tad to zimes
        var cur_data = find_fcfs_data(starta_poz, pieprasijumi);
        var total_dist = find_total_dist(cur_data);
        var total_dist_arr = find_total_dist_arr(cur_data);
        
        my_scatter_chart_data_sets.push({
            label: "FCFS",
            data: add_x_koord(cur_data),
            backgroundColor: "blue",
            borderColor: "lightblue",
            fill: false,
            lineTension: 0,
            showLine: true,
            radius: 5
        });
        
        my_bar_chart_data_sets.push({
            label: "FCFS",
            data: [total_dist],
            backgroundColor: ["blue"],
            borderColor: ["lightblue"],
            borderWidth: 1
            
        });
        
        my_scatter_chart2_data_sets.push({
            label: "FCFS",
            data: add_x_koord(total_dist_arr),
            backgroundColor: "blue",
            borderColor: "lightblue",
            fill: false,
            lineTension: 0,
            showLine: true,
            radius: 5
        });
    }//end of if
    
    if($("#SSTF_alg_chk_box").prop("checked")){
        //ja ir atzimets SSTF, tad to zimes
        var cur_data = find_sstf_data(starta_poz, pieprasijumi);
        var total_dist = find_total_dist(cur_data);
        var total_dist_arr = find_total_dist_arr(cur_data);
        
        my_scatter_chart_data_sets.push({
            label: "SSTF",
            data: add_x_koord(cur_data),
            backgroundColor: "green",
            borderColor: "lightgreen",
            fill: false,
            lineTension: 0,
            showLine: true,
            radius: 5
        });
        
        my_bar_chart_data_sets.push({
            label: "SSTF",
            data: [total_dist],
            backgroundColor: ["green"],
            borderColor: ["lightgreen"],
            borderWidth: 1
            
        });
        
        my_scatter_chart2_data_sets.push({
            label: "SSTF",
            data: add_x_koord(total_dist_arr),
            backgroundColor: "green",
            borderColor: "lightgreen",
            fill: false,
            lineTension: 0,
            showLine: true,
            radius: 5
        });
    }
    
    
    if($("#SCAN_alg_chk_box").prop("checked")){
        //ja ir atzimets SCAN, tad to zimes
        var cur_data = find_scan_data(starta_poz, pieprasijumi);
        var total_dist = find_total_dist(cur_data);
        var total_dist_arr = find_total_dist_arr(cur_data);
        
        my_scatter_chart_data_sets.push({
            label: "SCAN",
            data: add_x_koord(cur_data),
            backgroundColor: "yellow",
            borderColor: "yellow",
            fill: false,
            lineTension: 0,
            showLine: true,
            radius: 5
        });
        
        my_bar_chart_data_sets.push({
            label: "SCAN",
            data: [total_dist],
            backgroundColor: ["yellow"],
            borderColor: ["yellow"],
            borderWidth: 1
            
        });
        
        my_scatter_chart2_data_sets.push({
            label: "SCAN",
            data: add_x_koord(total_dist_arr),
            backgroundColor: "yellow",
            borderColor: "yellow",
            fill: false,
            lineTension: 0,
            showLine: true,
            radius: 5
        });
    }
    if($("#CSCAN_alg_chk_box").prop("checked")){
        //ja ir atzimets CSCAN, tad to zimes
        var cur_data = find_cscan_data(starta_poz, pieprasijumi);
        var total_dist = find_total_dist(cur_data);
        var total_dist_arr = find_total_dist_arr(cur_data);
        
        my_scatter_chart_data_sets.push({
            label: "C-SCAN",
            data: add_x_koord(cur_data),
            backgroundColor: "red",
            borderColor: "red",
            fill: false,
            lineTension: 0,
            showLine: true,
            radius: 5
        });
        
        my_bar_chart_data_sets.push({
            label: "C-SCAN",
            data: [total_dist],
            backgroundColor: ["red"],
            borderColor: ["red"],
            borderWidth: 1
        });
        
        my_scatter_chart2_data_sets.push({
            label: "C-SCAN",
            data: add_x_koord(total_dist_arr),
            backgroundColor: "red",
            borderColor: "red",
            fill: false,
            lineTension: 0,
            showLine: true,
            radius: 5
        });
    }
    if($("#CLOOK_alg_chk_box").prop("checked")){
        //ja ir atzimets CLOOK, tad to zimes
        var cur_data = find_clook_data(starta_poz, pieprasijumi);
        var total_dist = find_total_dist(cur_data);
        var total_dist_arr = find_total_dist_arr(cur_data);
        
        my_scatter_chart_data_sets.push({
            label: "C-LOOK",
            data: add_x_koord(cur_data),
            backgroundColor: "darkmagenta",
            borderColor: "darkmagenta",
            fill: false,
            lineTension: 0,
            showLine: true,
            radius: 5
        });
        
        my_bar_chart_data_sets.push({
            label: "C-LOOK",
            data: [total_dist],
            backgroundColor: ["darkmagenta"],
            borderColor: ["darkmagenta"],
            borderWidth: 1
        });
        
        my_scatter_chart2_data_sets.push({
            label: "C-LOOK",
            data: add_x_koord(total_dist_arr),
            backgroundColor: "darkmagenta",
            borderColor: "darkmagenta",
            fill: false,
            lineTension: 0,
            showLine: true,
            radius: 5
        });  
    }
    
    
    
    
    
    
    
    
    
    
    
    if(my_scatter_chart_data_sets.length<1){
        //nav nekas atzimets
        my_chart_container.className="hidden";
        my_chart_container.innerHTML = '&nbsp;';
        my_chart_container.innerHTML = '<canvas id="my_scatter_chart_canvas"></canvas><br><br>\n\
                                                                   <canvas id="my_bar_chart_canvas"></canvas><br><br>\n\
                                                                   <canvas id="my_scatter_chart2_canvas"></canvas>';
    
        return;
    }
    
    
    
    
    
    
    
    
    
    //scatter chart data
    var scatter_chart_data = {
      datasets: my_scatter_chart_data_sets
    };

    //options
    var scatter_chart_options = {
      responsive: true,
      title: {
        display: true,
        position: "top",
        text: "Grafiks",
        fontSize: 18,
        fontColor: "#111"
      },
      legend: {
        display: true,
        position: "bottom",
        labels: {
          fontColor: "#333",
          fontSize: 16
        }
      },
      scales: {
        yAxes: [{
            ticks: {
                suggestedMax: parseInt(document.getElementById("max_robeza").value)+20
            }
        }]
      }
    };

    //create Chart class object
    var scatter_chart = new Chart(ctx_scatter_chart, {
      type: "scatter",
      data: scatter_chart_data,
      options: scatter_chart_options
    });
    
    
    
    
    
    //bar chart data
    var bar_chart_data = {
      labels: ["total dist"],
      datasets: my_bar_chart_data_sets
    };

    //options
    var bar_chart_options = {
        responsive: true,
        title: {
          display: true,
          position: "top",
          text: "Bar Graph of total dist",
          fontSize: 18,
          fontColor: "#111"
        },
        legend: {
          display: true,
          position: "bottom",
          labels: {
            fontColor: "#333",
            fontSize: 16
          }
        }
    };

    //create Chart class object
    var bar_chart = new Chart(ctx_bar_chart, {
      type: "bar",
      data: bar_chart_data,
      options: bar_chart_options
    });
    
    
    
    
    //scatter chart data
    var scatter_chart2_data = {
      datasets: my_scatter_chart2_data_sets
    };

    //options
    var scatter_chart2_options = {
      responsive: true,
      title: {
        display: true,
        position: "top",
        text: "Grafiks",
        fontSize: 18,
        fontColor: "#111"
      },
      legend: {
        display: true,
        position: "bottom",
        labels: {
          fontColor: "#333",
          fontSize: 16
        }
      }
    };
    
    

    //create Chart class object
    var line_chart2 = new Chart(ctx_scatter_chart2, {
      type: "scatter",
      data: scatter_chart2_data,
      options: scatter_chart2_options
    });
    
    
    
    
}// end of myChartDraw
















//function myChartDraw(starta_poz, pieprasijumi){
//    
//    var ctx_line_chart = $("#my_line_chart_canvas"); //tas pats kas document.getElementById
//    var ctx_bar_chart = $("#my_bar_chart_canvas"); //tas pats kas document.getElementById
//    var ctx_line_chart2 = $("#my_line_chart2_canvas");
//    
//    var N = pieprasijumi.length;
//    var my_line_chart_labels = [];
//    for(i=0; i<N+1; ++i)my_line_chart_labels.push(i);
//    
//    var my_line_chart_data_sets=[];
//    var my_line_chart2_data_sets=[];
//    
//    
//    
//    var my_bar_chart_data_sets=[];
//    
//    
//    if($("#FCFS_alg_chk_box").prop("checked")){
//        //ja ir atzimets FCFS, tad to zimes
//        var cur_data = find_fcfs_data(starta_poz, pieprasijumi);
//        var total_dist = find_total_dist(cur_data);
//        var total_dist_arr = find_total_dist_arr(cur_data);
//        
//        my_line_chart_data_sets.push({
//            label: "FCFS",
//            data: cur_data,
//            backgroundColor: "blue",
//            borderColor: "lightblue",
//            fill: false,
//            lineTension: 0,
//            radius: 5
//        });
//        
//        my_bar_chart_data_sets.push({
//            label: "FCFS",
//            data: [total_dist],
//            backgroundColor: ["blue"],
//            borderColor: ["lightblue"],
//            borderWidth: 1
//            
//        });
//        
//        my_line_chart2_data_sets.push({
//            label: "FCFS",
//            data: total_dist_arr,
//            backgroundColor: "blue",
//            borderColor: "lightblue",
//            fill: false,
//            lineTension: 0,
//            radius: 5
//        });
//    }//end of if
//    
//    if($("#SSTF_alg_chk_box").prop("checked")){
//        //ja ir atzimets SSTF, tad to zimes
//        var cur_data = find_sstf_data(starta_poz, pieprasijumi);
//        var total_dist = find_total_dist(cur_data);
//        var total_dist_arr = find_total_dist_arr(cur_data);
//        
//        my_line_chart_data_sets.push({
//            label: "SSTF",
//            data: cur_data,
//            backgroundColor: "green",
//            borderColor: "lightgreen",
//            fill: false,
//            lineTension: 0,
//            radius: 5
//        });
//        
//        my_bar_chart_data_sets.push({
//            label: "SSTF",
//            data: [total_dist],
//            backgroundColor: ["green"],
//            borderColor: ["lightgreen"],
//            borderWidth: 1
//            
//        });
//        
//        my_line_chart2_data_sets.push({
//            label: "SSTF",
//            data: total_dist_arr,
//            backgroundColor: "green",
//            borderColor: "lightgreen",
//            fill: false,
//            lineTension: 0,
//            radius: 5
//        });
//    }
//    
//    
//    if($("#SCAN_alg_chk_box").prop("checked")){
//        //ja ir atzimets SCAN, tad to zimes
//        var cur_data = find_scan_data(starta_poz, pieprasijumi);
//        var total_dist = find_total_dist(cur_data);
//        var total_dist_arr = find_total_dist_arr(cur_data);
//        
//        my_line_chart_data_sets.push({
//            label: "SCAN",
//            data: cur_data,
//            backgroundColor: "yellow",
//            borderColor: "yellow",
//            fill: false,
//            lineTension: 0,
//            radius: 5
//        });
//        
//        my_bar_chart_data_sets.push({
//            label: "SCAN",
//            data: [total_dist],
//            backgroundColor: ["yellow"],
//            borderColor: ["yellow"],
//            borderWidth: 1
//            
//        });
//        
//        my_line_chart2_data_sets.push({
//            label: "SCAN",
//            data: total_dist_arr,
//            backgroundColor: "yellow",
//            borderColor: "yellow",
//            fill: false,
//            lineTension: 0,
//            radius: 5
//        });
//    }
//    if($("#CSCAN_alg_chk_box").prop("checked")){
//        //ja ir atzimets CSCAN, tad to zimes
//        var cur_data = find_cscan_data(starta_poz, pieprasijumi);
//        var total_dist = find_total_dist(cur_data);
//        var total_dist_arr = find_total_dist_arr(cur_data);
//        
//        my_line_chart_data_sets.push({
//            label: "C-SCAN",
//            data: cur_data,
//            backgroundColor: "red",
//            borderColor: "red",
//            fill: false,
//            lineTension: 0,
//            radius: 5
//        });
//        
//        my_bar_chart_data_sets.push({
//            label: "C-SCAN",
//            data: [total_dist],
//            backgroundColor: ["red"],
//            borderColor: ["red"],
//            borderWidth: 1
//        });
//        
//        my_line_chart2_data_sets.push({
//            label: "C-SCAN",
//            data: total_dist_arr,
//            backgroundColor: "red",
//            borderColor: "red",
//            fill: false,
//            lineTension: 0,
//            radius: 5
//        });
//    }
//    if($("#CLOOK_alg_chk_box").prop("checked")){
//        //ja ir atzimets CLOOK, tad to zimes
//        var cur_data = find_clook_data(starta_poz, pieprasijumi);
//        var total_dist = find_total_dist(cur_data);
//        var total_dist_arr = find_total_dist_arr(cur_data);
//        
//        my_line_chart_data_sets.push({
//            label: "C-LOOK",
//            data: cur_data,
//            backgroundColor: "darkmagenta",
//            borderColor: "darkmagenta",
//            fill: false,
//            lineTension: 0,
//            radius: 5
//        });
//        
//        my_bar_chart_data_sets.push({
//            label: "C-LOOK",
//            data: [total_dist],
//            backgroundColor: ["darkmagenta"],
//            borderColor: ["darkmagenta"],
//            borderWidth: 1
//        });
//        
//        my_line_chart2_data_sets.push({
//            label: "C-LOOK",
//            data: total_dist_arr,
//            backgroundColor: "darkmagenta",
//            borderColor: "darkmagenta",
//            fill: false,
//            lineTension: 0,
//            radius: 5
//        });  
//    }
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    if(my_line_chart_data_sets.length<1){
//        //nav nekas atzimets
//        document.getElementById("my_chart_container").innerHTML = '&nbsp;';
//        document.getElementById("my_chart_container").innerHTML = '<canvas id="my_line_chart_canvas"></canvas><br>\n\
//                                                                   <canvas id="my_bar_chart_canvas"></canvas><br>\n\
//                                                                   <canvas id="my_line_chart2_canvas"></canvas>';//izmet ieprieksejo chart
//        return;
//    }
//    
//    
//    
//    
//    
//    /*
//    //datasets
//    var my_data_sets=[
//        {
//          label: "TeamA Score",
//          data: [10, 50, 25, 70, 40],
//          backgroundColor: "blue",
//          borderColor: "lightblue",
//          fill: false,
//          lineTension: 0,
//          radius: 5
//        },
//        {
//        label: "TeamB Score",
//        data: [20, 35, 40, 60, 50],
//        backgroundColor: "green",
//        borderColor: "lightgreen",
//        fill: false,
//        lineTension: 0,
//        radius: 5
//        }
//    ];
//    */
//    
//    
//    //line chart data
//    var line_chart_data = {
//      labels: my_line_chart_labels,
//      datasets: my_line_chart_data_sets
//    };
//
//    //options
//    var line_chart_options = {
//      responsive: true,
//      title: {
//        display: true,
//        position: "top",
//        text: "Grafiks",
//        fontSize: 18,
//        fontColor: "#111"
//      },
//      legend: {
//        display: true,
//        position: "bottom",
//        labels: {
//          fontColor: "#333",
//          fontSize: 16
//        }
//      }
//    };
//
//    //create Chart class object
//    var line_chart = new Chart(ctx_line_chart, {
//      type: "line",
//      data: line_chart_data,
//      options: line_chart_options
//    });
//    
//    
//    
//    
//    
//    //bar chart data
//    var bar_chart_data = {
//      labels: ["total dist"],
//      datasets: my_bar_chart_data_sets
//    };
//
//    //options
//    var bar_chart_options = {
//        responsive: true,
//        title: {
//          display: true,
//          position: "top",
//          text: "Bar Graph of total dist",
//          fontSize: 18,
//          fontColor: "#111"
//        },
//        legend: {
//          display: true,
//          position: "bottom",
//          labels: {
//            fontColor: "#333",
//            fontSize: 16
//          }
//        },
//    };
//
//    //create Chart class object
//    var bar_chart = new Chart(ctx_bar_chart, {
//      type: "bar",
//      data: bar_chart_data,
//      options: bar_chart_options
//    });
//    
//    
//    
//    
//    //line chart data
//    var line_chart2_data = {
//      labels: my_line_chart_labels, //nem tos pasus labels
//      datasets: my_line_chart2_data_sets
//    };
//
//    //options
//    var line_chart2_options = {
//      responsive: true,
//      title: {
//        display: true,
//        position: "top",
//        text: "Grafiks",
//        fontSize: 18,
//        fontColor: "#111"
//      },
//      legend: {
//        display: true,
//        position: "bottom",
//        labels: {
//          fontColor: "#333",
//          fontSize: 16
//        }
//      }
//    };
//
//    //create Chart class object
//    var line_chart2 = new Chart(ctx_line_chart2, {
//      type: "line",
//      data: line_chart2_data,
//      options: line_chart2_options
//    });
//    
//    
//    
//    
//}// end of myChartDraw
