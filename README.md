# HDD plānošanas (*scheduling*) algoritmu vizualizācija

Šajā projektā tiek vizualizēti vairāki HDD plānošanas algoritmi ar bootstrap priekš lapas un javascript un Chart.js priekš vizualizācijas.

#### Kopumā apskatīti 5 dažādi varianti:

1. FCFS - first come, first serve
2. SSTF - shortest seek time first
3. SCAN
4. C-SCAN
5. C-LOOK

------
Algoritmu apraksti ņemti no http://www.cs.iit.edu/~cs561/cs450/disksched/disksched.html

*Skatīt failu:* **DISK SCHEDULING ALGORITHMS.html**

------
Gatavo lapu var redzēt: https://csparrow.gitlab.io/hdd_scheduling_algorithm_visualization/


